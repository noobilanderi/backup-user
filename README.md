# backup-user
Bash shell script that aims to make using rsync easier when copying data to multiple backup locations.

### Requirements
- rsync
- findmnt (in most distros found in util-linux package, should already be installed in most distros)

## Usage
```
backup-user -h
backup-user -o=<label> -i=<label>
backup-user --output=<label> --input=<label>
```

Set up your `~/.config/backup-user.config` by adding input and output lines.

*  You can add as many input and output locations as you want into config.

```
input;<give label>;<absolute path>
output;<give label>;<absolute path to device base folder>
```

### Example
For user John who has external HDD mounted in `/run/media/John/HDD`

##### ~/.config/backup-user.config
```
input;home;/home/John
output;myHDD;/run/media/John/HDD
```

##### backup-user
```
backup-user --input=home --output=myHDD
```
